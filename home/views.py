from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def home_page(request):
    return render(request, 'home_page.html')

def logout_page(request):
    logout(request)
    return redirect('home:login_page')

def login_page(request):
    if(request.method == 'POST'):
        username = request.POST['username']
        password = request.POST['password']
        auth = authenticate(request, username = username, password = password)
        if(auth is not None):
            login(request, auth)
            return redirect("home:home_page")
        return render(request, 'login_page.html', { 'errors' : "Invalid username or password"})
    return render(request, 'login_page.html')

def register(request):
    form = UserCreationForm
    if(request.method == 'POST'):
        form = form(request.POST)
        if(form.is_valid()):
            form.save()
            return redirect("home:login_page")
        return render(request, 'register.html', {'form' : form})
    context = {
        'form' : form()
    }
    return render(request, 'register.html', context)