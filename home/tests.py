from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth import login, logout
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time

class AccountTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # super(TwitterLiteTest, cls).setUpClass()
        super().setUpClass()



    def test_index(self):
        index = Client().get('/')
        self.assertEqual(index.status_code, 302)

    def test_login(self):
        index = Client().get('/login/')
        self.assertEqual(index.status_code, 200)
    
    def test_logout(self):
        index = Client().get('/logout/')
        self.assertEqual(index.status_code, 302)



    def test_sign_in(self):
        index = Client().post('/register/', data = {'username' : 'coba', 'password1' : 'cobacoba', 'password2' : 'cobacoba'})
        index = Client().post('/login/', data = { 'username': 'coba', 'password' : 'cobacoba'}, follow=True)
        self.assertIn('Logout', index.content.decode())

    def test_invalid_login(self):
        index = Client().post('/login/', data = { 'username': 'coba', 'password' : 'coba'}, follow=True)
        self.assertIn('Invalid username or password', index.content.decode())

    def test_invalid_register(self):
        index = Client().post('/register/', data = {'username' : 'coba', 'password1' : 'coba', 'password2' : 'coba'})
        self.assertIn('The password is too similar to the username.', index.content.decode())


    def test_login_button(self):
        index = Client().get("/login/")
        self.assertIn("Login", index.content.decode())
    
    def test_register_button(self):
        index = Client().get("/login/")
        self.assertIn("Register", index.content.decode())

    def test_register_form(self):
        index = Client().get("/register/")
        self.assertIn("<form", index.content.decode())
    
    def test_login_form(self):
        index = Client().get("/login/")
        self.assertIn("<form", index.content.decode())
