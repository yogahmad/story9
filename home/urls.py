from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home_page, name = 'home_page'),
    path('register/', views.register, name = 'register'),
    path('login/', views.login_page, name = 'login_page'),
    path('logout/', views.logout_page, name = 'logout_page'),
]